package com.hcl.util;

import java.util.concurrent.TimeUnit;

import com.hcl.testbase.Testbase;

public class TestUtility extends Testbase {

	public static void openurl()
	{
		String url=repo.getProperty("url");
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(4000,TimeUnit.SECONDS);
	}
}
